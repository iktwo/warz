QT = quick widgets svg

SOURCES += \
    src/main.cpp \
    src/gamemodel.cpp \
    src/player.cpp \
    src/soldier.cpp \
    src/tile.cpp \
    src/unit.cpp \
    src/structure.cpp

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    qml/main.qml \
    qml/GameArea.qml \
    qml/MovementHighlighter.qml

HEADERS += \
    src/gamemodel.h \
    src/player.h \
    src/soldier.h \
    src/tile.h \
    src/unit.h \
    src/structure.h
