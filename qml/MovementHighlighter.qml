import QtQuick 2.0

Rectangle {
    id: root

    height: parent.height * 1.5
    width: parent.width * 1.5

    anchors.centerIn: parent

    rotation: 45

    antialiasing: true

    clip: true

    states: [
        State {
            name: "1"
            PropertyChanges { target: g1; color: "#11FFFFFF" }
            PropertyChanges { target: g2; color: "#77000000" }
            PropertyChanges { target: g3; color: "#44000000" }
            PropertyChanges { target: g4; color: "#11ffffff" }
            PropertyChanges { target: g5; color: "#44000000" }
            PropertyChanges { target: g6; color: "#77000000" }
            PropertyChanges { target: g7; color: "#11FFFFFF" }
        },
        State {
            name: "2"
            PropertyChanges { target: g1; color: "#11000000" }
            PropertyChanges { target: g2; color: "#77FFFFFF" }
            PropertyChanges { target: g3; color: "#44FFFFFF" }
            PropertyChanges { target: g4; color: "#11000000" }
            PropertyChanges { target: g5; color: "#44FFFFFF" }
            PropertyChanges { target: g6; color: "#77FFFFFF" }
            PropertyChanges { target: g7; color: "#11000000" }
        }
    ]

    MouseArea {
        anchors.fill: parent

        onClicked: {
            if(root.state == "1")
                root.state = "2"
            else
                root.state = "1"

        }
    }

    gradient: Gradient {

        GradientStop { id: g1; position: 0.0; color: "#11FFFFFF"; Behavior on color { ColorAnimation { } } }
        GradientStop { id: g2; position: 0.125; color: "#77000000"; Behavior on color { ColorAnimation { } } }
        GradientStop { id: g3; position: 0.25; color: "#44000000"; Behavior on color { ColorAnimation { } } }
        GradientStop { id: g4; position: 0.5; color: "#11ffffff"; Behavior on color { ColorAnimation { } } }
        GradientStop { id: g5; position: 0.75; color: "#44000000"; Behavior on color { ColorAnimation { } } }
        GradientStop { id: g6; position: 0.875; color: "#77000000"; Behavior on color { ColorAnimation { } } }
        GradientStop { id: g7; position: 1.0; color: "#11FFFFFF"; Behavior on color { ColorAnimation { } } }

//        GradientStop { position: 0.0; color: "#11000000" }
//        GradientStop { position: 0.125; color: "#77FFFFFF" }
//        GradientStop { position: 0.25; color: "#44FFFFFF" }
//        GradientStop { position: 0.5; color: "#11000000" }
//        GradientStop { position: 0.75; color: "#44FFFFFF" }
//        GradientStop { position: 0.875; color: "#77FFFFFF" }
//        GradientStop { position: 1.0; color: "#11000000" }

        //        GradientStop { id: g1; position: 0;    color: "#88FFFFFF" }
        //        GradientStop { id: g8; position: 0.1; color: "#11000000" }
        //        GradientStop { id: g7; position: 0.2;   color: "#11FFFFFF" }
        //        GradientStop { id: g2; position: 0.4;   color: "#55FFFFFF" }
        //        GradientStop { id: g3; position: 0.5;   color: "#33FFFFFF" }
        //        GradientStop { id: g4; position: 0.501; color: "#11000000" }
        //        GradientStop { id: g5; position: 0.8;   color: "#11FFFFFF" }
        //        GradientStop { id: g6; position: 1;    color: "#55FFFFFF" }
    }

    //    Rectangle {
    //        id: bar1

    //        width: Math.sqrt((parent.width * parent.width) + (parent.height * parent.height))
    //        height: 10

    //        anchors.horizontalCenter: parent.horizontalCenter
    //        //anchors.centerIn: parent

    //        rotation: 45

    //        color: "black"

    //        //Behavior on y { }
    //    }

//    Timer {
//        interval: 16
//        repeat: true
//        triggeredOnStart: true
//        running: true
//        onTriggered: {
//            //            bar1.y += 1
//        }
//    }
}
