import QtQuick 2.0

Item {
    GridView {
        id: view

        anchors.fill: parent
        model: gameModel

        cellHeight: height / gameModel.rows
        cellWidth: width / gameModel.columns

        delegate: Rectangle {
            height: view.cellHeight
            width: view.cellWidth

            border.width: 2
            border.color: "gray"

            Image {
                anchors.fill: parent
                anchors.margins: 2

                fillMode: Image.PreserveAspectCrop
                source: "qrc:/images/grass"
            }

            Image {
//                anchors.fill: parent
//                anchors.margins: 2
                height: parent.height * 1.5
                width: parent.width
                anchors.bottom: parent.bottom
                fillMode: Image.PreserveAspectFit


                source: model.structure != undefined ? "qrc:/images/mountain" : ""
            }

            Image {
                anchors {
                    fill: parent
                    margins: parent.height * 0.02
                }

                fillMode: Image.PreserveAspectFit
                /// TODO: check if player is valid or think how to always have a player
                source: model.unit != undefined  && model.unit.value == 1 ? "qrc:/images/player" + model.player + ".svg" : ""
                antialiasing: true
            }

            Rectangle {
                anchors.fill: parent
                color: "black"
                opacity: 0.2
                visible: model.unit != undefined && model.unit.hasMoved
            }

            MouseArea {
                anchors.fill: parent

                /// TODO: decide click should be handled on enemys unit or not
                enabled: model.unit != undefined && !model.unit.hasMoved && model.unit.value == 1 //&& model.player != lastPlayer

                onClicked: {
                    if (model.player != activePlayer)
                        return;

                    //model.hasMoved = true
                    currentHighlightedPiece = index
                    gameModel.calculatePossibleMoves(index)
                }
            }
        }
    }

    GridView {
        id: view2

        anchors.fill: parent
        model: gameModel

        cellHeight: height / gameModel.rows
        cellWidth: width / gameModel.columns
        interactive: false

        delegate: Item  {
            height: view2.cellHeight
            width: view2.cellWidth

            clip: true

            visible: gameModel.validMoves.indexOf(index) != -1

            Rectangle {
                anchors.fill: parent

                /// TODO: set color according to possible value in that spot
                color: "purple"
                opacity: parent.visible ? 0.22 : 0

                /// TODO: fix animation when click on an already visible item
                Behavior on opacity { NumberAnimation { duration: 250 } }
            }

            Rectangle {
                height: parent.height * 2
                width: parent.width * 2

                smooth: true
                anchors.centerIn: parent

                rotation: 45

                gradient: Gradient {
                    GradientStop { position: 0;    color: "#88FFFFFF" }
                    GradientStop { position: 0.1;   color: "#55FFFFFF" }
                    GradientStop { position: 0.5;   color: "#33FFFFFF" }
                    GradientStop { position: 0.501; color: "#11000000" }
                    GradientStop { position: 0.8;   color: "#11FFFFFF" }
                    GradientStop { position: 1;    color: "#55FFFFFF" }
                }
            }

            MouseArea {
                anchors.fill: parent

                enabled: parent.visible

                onClicked: {
                    /// TODO: think of how to animate this
                    /// TODO retrieve possible actions for this new place
//                    gameModel.retrieveActions(index)
                    gameModel.move(currentHighlightedPiece, index)
                }
            }
        }
        //        delegate: Rectangle {
        //            height: view2.cellHeight
        //            width: view2.cellWidth

        //            opacity: 0.3
        //            color: "purple"
        //            visible: possibleMoves.indexOf(index) != -1

        //            border.width: 2
        //            border.color: "black"
        //        }
    }
}
