import QtQuick 2.1
import QtQuick.Controls 1.0

ApplicationWindow {
    id: root

    property variant possibleMoves: []

    /// TODO: find a better name for this
    property int currentHighlightedPiece

    property int activePlayer: 1

    title: gameModel.rows + " x " + gameModel.columns + " PLAYER: " + activePlayer
    width: 640
    height: 640

    StackView {
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: statusBar.top
        }

        initialItem: gameArea
    }

    GameArea {
        id: gameArea
    }

    Rectangle {
        id: statusBar

        anchors.bottom: parent.bottom

        height: 80
        width: parent.width

        Button {
            anchors.fill: parent
            text: "End turn"
            onClicked: {
                gameModel.resetMovesForPlayer(activePlayer)

                if (activePlayer == 1)
                    activePlayer = 2
                else
                    activePlayer = 1
            }
        }
    }


//    MovementHighlighter {

//    }

    //    menuBar: MenuBar {
    //        Menu {
    //            title: qsTr("File")
    //            MenuItem {
    //                text: qsTr("Exit")
    //                onTriggered: Qt.quit();
    //            }
    //        }
    //    }

    //    Button {
    //        text: qsTr("Hello World")
    //        anchors.centerIn: parent
    //    }
}
