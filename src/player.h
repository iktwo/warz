#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>

class Player : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id READ id NOTIFY idChanged)
//    Q_PROPERTY(QColor color READ color NOTIFY colorChanged)
public:
    explicit Player(int id, QObject *parent = 0);

    int id() const;

signals:
    void idChanged();

public slots:

private:
    int mId;

};

#endif // PLAYER_H
