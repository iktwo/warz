#include "unit.h"

Unit::Unit(QObject *parent) :
    QObject(parent), mPlayerId(0), mHasMoved(false)
{
}

bool Unit::hasMoved() const
{
    return mHasMoved;
}

void Unit::setHasMoved(bool hasMoved)
{
    if (mHasMoved == hasMoved)
        return;

    mHasMoved = hasMoved;
    emit hasMovedChanged();
}

int Unit::health() const
{
    return mHealth;
}

int Unit::playerId() const
{
    return mPlayerId;
}

void Unit::setPlayerId(int player)
{
    if (mPlayerId == player)
        return;

    mPlayerId = player;
    emit playerIdChanged();
}
