#include <QApplication>
#include <QQuickView>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QSettings>
#include <QDebug>

#include "gamemodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Iktwo");
    QCoreApplication::setOrganizationDomain("iktwo.com");
    QCoreApplication::setApplicationName("Warz");

    QScopedPointer<QApplication> app(new QApplication(argc, argv));

    QQmlEngine engine;
    qmlRegisterType<Tile>();

    QObject::connect(&engine, SIGNAL(quit()), QCoreApplication::instance(), SLOT(quit()));

    GameModel gameModel;
    engine.rootContext()->setContextProperty("gameModel", &gameModel);

    QQmlComponent component(&engine, QUrl("qrc:/qml/qml/main.qml"));

    if (!component.isReady())
        qWarning("%s", qPrintable(component.errorString()));

    QObject *topLevel = component.create();

    QQuickWindow *window = qobject_cast<QQuickWindow *>(topLevel);

    window->show();

    return app->exec();
}
