#include "gamemodel.h"

#include <QDebug>

GameModel::GameModel(QObject *parent) :
    QAbstractListModel(parent), mRows(0), mColumns(0)
{
    for (int rows = 0; rows < 7; ++rows) {
        mData.push_back(new QVector<Tile*>());

        for (int columns = 0; columns < 5; ++columns) {
            mData.at(rows)->push_back(new Tile());
        }
    }

    //    QVector<QVector<int> *> mData;
    //    mData.push_back(new QVector<int>());
    //    mData.at(0)->push_back(0);

    Soldier *s1 = new Soldier();
    Soldier *s2 = new Soldier();

    //    Soldier *s3 = new Soldier();
    //    Soldier *s4 = new Soldier();

    mPlayers.append(new Player(1));
    mPlayers.append(new Player(2));

    s1->setPlayerId(1);
    s2->setPlayerId(2);


    Structure *mountain = new Structure();

    (*mData[1])[2]->setStructure(mountain);
    (*mData[1])[3]->setStructure(mountain);
    (*mData[1])[4]->setStructure(mountain);
    (*mData[2])[2]->setStructure(mountain);
    (*mData[2])[3]->setStructure(mountain);
    (*mData[2])[4]->setStructure(mountain);

    (*mData[1])[1]->setUnit(s1);
    (*mData[3])[2]->setUnit(s2);

    mRows = mData.size();
    if (mRows > 0)
        mColumns = mData.at(0)->size();

    //    mData[2][3] = 1;
    //    mData[7][3] = 1;
}

int GameModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    int size = mData.size();

    if (mData.size() > 0)
        size = size * mData.at(0)->size();

    return size;
}

QVariant GameModel::data(const QModelIndex &index, int role) const
{
//    qDebug() << "QUERYING DATA for:" << index.row();
    int size = mData.size();
    if (mData.size() > 0) {
        size = size * mData.at(0)->size();
    } else {
        return QVariant();
    }

    if (index.row() < 0 || index.row() >= size)
        return QVariant();

    switch (role) {
    case UnitRole:
        if (mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit())
            return QVariant::fromValue(mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit());
        break;
    case StructureRole:
        if (mData.at(index.row() / mColumns)->at(index.row() % mColumns)->structure())
            return QVariant::fromValue(mData.at(index.row() / mColumns)->at(index.row() % mColumns)->structure());
        break;
    case PlayerRole:
        if (mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit())
            if (mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit()->playerId())
                return mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit()->playerId();
        break;
    }

    return QVariant();
}

//bool GameModel::setData(const QModelIndex &index, const QVariant &value, int role)
//{
//    int size = mData.size();
//    if (mData.size() > 0) {
//        size = size * mData.at(0)->size();
//    } else {
//        return false;
//    }

//    if (index.row() < 0 || index.row() >= size)
//        return false;

//    switch (role) {
////    case UnitRole:
////        if (mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit())
////            return QVariant::fromValue(mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit());
////        break;

////    case PlayerRole:
////        if (mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit())
////            if (mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit()->playerId())
////                return mData.at(index.row() / mColumns)->at(index.row() % mColumns)->unit()->playerId();
////        break;
//    case HasMovedRole:
//        qDebug() << "has moved role";
//        break;
//    }

//    return false;
//}

QList<int> GameModel::validMoves() const
{
    return mValidMoves;
}

int GameModel::rows() const
{
    return mRows;
}

int GameModel::columns() const
{
    return mColumns;
}

int GameModel::count() const
{
    return mRows * mColumns;
}

void GameModel::calculatePossibleMoves(int index)
{
    // qDebug() << Q_FUNC_INFO << "calculating possible moves";
    int row = index / mColumns;
    int column = index % mColumns;

    mValidMoves.clear();
    int distance = 2;

    for (int j = -distance; j < distance + 1; ++j) {
        if (row + j >= 0 && row + j < mRows) {
            for (int i = -(distance - abs(j)); i < (distance - abs(j)) + 1; ++i) {
                if (column + i >= 0 && column + i < mColumns)
                    mValidMoves.append(column + i + ((row + j) * mColumns));
            }
        }
    }

    /// TODO: go through the list and remove if another entity is there
    // qDebug() << "Valid moves:" << validPositions;
    emit validMovesChanged();
}

//void GameModel::Check

bool GameModel::move(int oldIndex, int newIndex)
{
    //    qDebug() << Q_FUNC_INFO << oldIndex << " - " << newIndex ;
    beginResetModel();

    /// TODO: check if valid move..

    int oldRow = oldIndex / mColumns;
    int oldColumn = oldIndex % mColumns;

    int newRow = newIndex / mColumns;
    int newColumn = newIndex % mColumns;

    (*mData[newRow])[newColumn]->setUnit((*mData[oldRow])[oldColumn]->unit());
    (*mData[newRow])[newColumn]->unit()->setHasMoved(true);
    (*mData[oldRow])[oldColumn]->setUnit(NULL);

    endResetModel();

    mValidMoves.clear();
    emit validMovesChanged();

    /// TODO: figure out why this doesn't work
    //    QModelIndex indx1 = createIndex(oldIndex - 1, 0);
    //    QModelIndex indx2 = createIndex(oldIndex + 1, 0);
    //    dataChanged(indx1, indx2);

    ////    /// TODO: maybe trigger animation here, between the 2 dataChanged()

    //    indx1 = createIndex(newIndex - 1, 0);
    //    indx2 = createIndex(newIndex + 1, 0);
    //    dataChanged(indx1, indx2);

    return true;
}

QList<int> GameModel::retrieveActions(int index)
{
    int row = index / mColumns;
    int column = index % mColumns;

    QList<int> actions;
    actions.append(int(Wait));
    actions.append(int(Attack));
    return actions;

    /// If clicked on enemy move next to it

    /// TODO: implement this
    /// 1: Check if there's a vehicle to get inside it
    /// 2: check if there's a building to conquer
    /// 3: check if there's an enemy next to it
    /// 4: wait
    /// 5: if there's a piece than can move and attack remotely, check here
    /// 6: activate special switch
}

Tile *GameModel::itemAt(int index)
{
    int row = index / mColumns;
    int column = index % mColumns;

    return mData.at(row)->at(column);
}

void GameModel::resetMovesForPlayer(int player)
{
    beginResetModel();
//    qDebug() << Q_FUNC_INFO << "BEGIN RESET MOVES";

    for (int i = 0; i < mData.size(); ++i) {
        for (int j = 0; j < mData.at(i)->size(); ++j) {
            if ((*mData[i])[j]->unit()) {
                if ((*mData[i])[j]->unit()->playerId() == player)
                    (*mData[i])[j]->unit()->setHasMoved(false);
            }
        }
    }
//    qDebug() << Q_FUNC_INFO << "END RESET MOVES";
    endResetModel();
}

//void GameModel::setHasMoved(int index)
//{
//    beginResetModel();

//    int row = index / mColumns;
//    int column = index % mColumns;

//    (*mData[row])[column]->unit()->setHasMoved(true);

//    endResetModel();
//}

QHash<int, QByteArray> GameModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[UnitRole] = "unit";
    roles[PlayerRole] = "player";
    roles[HasMovedRole] = "hasMoved";
    roles[StructureRole] = "structure";
    return roles;
}
