#ifndef SOLDIER_H
#define SOLDIER_H

#include <QObject>

#include "unit.h"
/// TODO: separate this, soldier has life, power
/// vehicle has gasoline

class Soldier : public Unit
{
    Q_OBJECT

    Q_PROPERTY(int value READ value NOTIFY valueChanged)
public:
    explicit Soldier(QObject *parent = 0);

    int value() const;

signals:
    void valueChanged();

public slots:

private:
    int mValue;

};

#endif // SOLDIER_H
