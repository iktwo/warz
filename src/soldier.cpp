#include "soldier.h"

Soldier::Soldier(QObject *parent) :
    Unit(parent), mValue(1)
{
}

int Soldier::value() const
{
    return mValue;

}
