#include "tile.h"

Tile::Tile(QObject *parent) :
    QObject(parent), mUnit(NULL), mStructure(NULL)
{
}

Unit *Tile::unit() const
{
    return mUnit;
}

void Tile::setUnit(Unit *unit)
{
    if (mUnit == unit)
        return;

    mUnit = unit;
    emit unitChanged();
}

Structure *Tile::structure() const
{
    return mStructure;
}

void Tile::setStructure(Structure *structure)
{
    if (mStructure == structure)
        return;

    mStructure = structure;
    emit structureChanged();
}
