#include "player.h"

Player::Player(int id, QObject *parent) :
    QObject(parent), mId(id)
{
}

int Player::id() const
{
    return mId;
}
