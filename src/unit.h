#ifndef UNIT_H
#define UNIT_H

#include <QObject>

#include "player.h"

class Unit : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool hasMoved READ hasMoved WRITE setHasMoved NOTIFY hasMovedChanged)
    Q_PROPERTY(int health READ health NOTIFY healthChanged)
    Q_PROPERTY(int playerId READ playerId NOTIFY playerIdChanged)
    // Can it move?
    // Range of damage
    // Some units can move, then attack, some can only move or attack
public:
    explicit Unit(QObject *parent = 0);

    bool hasMoved() const;
    void setHasMoved(bool hasMoved);

    int health() const;

    int playerId() const;
    void setPlayerId(int playerId);

signals:
    void hasMovedChanged();
    void healthChanged();
    void playerIdChanged();

public slots:

private:
    int mPlayerId;
    bool mHasMoved;
    int mHealth;
};

#endif // UNIT_H
