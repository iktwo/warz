#ifndef TILE_H
#define TILE_H

#include <QObject>

#include "unit.h"
#include "structure.h"

class Tile : public QObject
{
    Q_OBJECT
    /// TODO: tile can hold a unit, a building/three, land

    Q_PROPERTY(Unit* unit READ unit NOTIFY unitChanged)
    Q_PROPERTY(Structure* structure READ structure NOTIFY structureChanged)
public:
    explicit Tile(QObject *parent = 0);

    Unit *unit() const;
    void setUnit(Unit *unit);

    Structure *structure() const;
    void setStructure(Structure *structure);

signals:
    void unitChanged();
    void structureChanged();

public slots:

private:
    Unit *mUnit;
    Structure *mStructure;

};

#endif // TILE_H
