#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include <QAbstractListModel>

#include "tile.h"
#include "soldier.h"
#include "player.h"

class GameModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int rows READ rows NOTIFY rowsChanged)
    Q_PROPERTY(int columns READ columns NOTIFY columnsChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(QList<int> validMoves READ validMoves NOTIFY validMovesChanged)

public:
    explicit GameModel(QObject *parent = 0);

    enum Roles {
        UnitRole = Qt::UserRole +1,
        StructureRole,
        PlayerRole,
        HasMovedRole
    };

    enum Actions {
        Wait,
        Attack
        // others.. conquer building, etc..
    };

    int rowCount(const QModelIndex &parent) const;

    QVariant data(const QModelIndex &index, int role) const;
//    bool setData(const QModelIndex &index, const QVariant &value, int role);

    QList<int> validMoves() const;

    int rows() const;
    int columns() const;
    int count() const;

    Q_INVOKABLE void calculatePossibleMoves(int index);
    Q_INVOKABLE bool move(int oldIndex, int newIndex);
    Q_INVOKABLE QList<int> retrieveActions(int index);
    Q_INVOKABLE Tile* itemAt(int index);
    Q_INVOKABLE void resetMovesForPlayer(int player);
//    Q_INVOKABLE void setHasMoved(int index);

protected:
    QHash<int, QByteArray> roleNames() const;

signals:
    void rowsChanged();
    void columnsChanged();
    void validMovesChanged();
    void countChanged();

public slots:

private:
    QVector<QVector<Tile*> *> mData;
    QList<int> mValidMoves;
    QList<Player *> mPlayers;
    int mRows;
    int mColumns;

};

#endif // GAMEMODEL_H
